  class Calc{
        
        public static String add(int a, int b){ 
          if (Double.valueOf(a)+Double.valueOf(b) > Integer.MAX_VALUE ||
          Double.valueOf(a)+Double.valueOf(b) < Integer.MIN_VALUE) return ("Too much for this type!");
          return Integer.toString(a+b); 
        }
        
        public String add(double a, double b){ 
          if (this.type == false) return ("Double is not supported!");
          if (Math.abs(a+b) == (Double.POSITIVE_INFINITY)) return ("Too much for this type!");
          return Double.toString(a+b); 
        }
        
        public static String sub(int a, int b){ 
          if (Double.valueOf(a)-Double.valueOf(b) > Integer.MAX_VALUE ||
          Double.valueOf(a)-Double.valueOf(b) < Integer.MIN_VALUE) return ("Too much for this type!");
          return Integer.toString(a-b); 
        }
        
        public String sub(double a, double b){ 
          if (this.type == false) return ("Double is not supported!");
          if (Math.abs(a-b) == (Double.POSITIVE_INFINITY)) return ("Too much for this type!");
          return Double.toString(a-b);
        }
        
        public static String mult(int a, int b){ 
          if (Double.valueOf(a)*Double.valueOf(b) > Integer.MAX_VALUE ||
          Double.valueOf(a)*Double.valueOf(b) < Integer.MIN_VALUE) return ("Too much for this type!");
          return Integer.toString(a*b); 
        }
        
        public String mult(double a, double b){ 
          if (this.type == false) return ("Double is not supported!");
          if (Math.abs(a*b) == (Double.POSITIVE_INFINITY)) return ("Too much for this type!");
          return Double.toString(a*b); 
        }
        
        public static String div(int a, int b){
          if (b==0) return ("You can't do this!");
          if (Double.valueOf(a)/Double.valueOf(b) > Integer.MAX_VALUE ||
          Double.valueOf(a)/Double.valueOf(b) < Integer.MIN_VALUE) return ("Too much for this type!");
          return Integer.toString(a/b); 
        }
        
        public String div(double a, double b){
          if (this.type == false) return ("Double is not supported!");
          if (b==0) return ("You can't do this!");
          if (Math.abs(a/b) == (Double.POSITIVE_INFINITY)) return ("Too much for this type!");
          return Double.toString(a/b); 
        }
        
        private boolean type = true;
        public boolean getType(){ return this.type; }
        public void setType(boolean a){ this.type = a; }
    }