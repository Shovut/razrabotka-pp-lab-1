class Main{
        
    public static void main(String[] args){
    
        testAddInt();
        testSubInt();
        testMultInt();
        testDivInt();
        
        
        testAddDouble();
        testSubDouble();
        testMultDouble();
        testDivDouble();
        
        return;
    }
    
    private static void testAddInt(){
        System.out.println("Addition Int testing:");
        
        if (Calc.add(0,0).equals("0"))
            log(true, "Test 1");
        else
            log(false, "Test 1");
        
        if (Calc.add(50 , 51).equals("101"))
            log(true, "Test 2");
        else
            log(false, "Test 2");
            
        if (Calc.add(Integer.MAX_VALUE, Integer.MAX_VALUE).equals("Too much for this type!"))
            log(true, "Test 3");
        else
            log(false, "Test 3");
    }
    
    private static void testSubInt(){
        System.out.println("Subtraction Int testing: ");
        
        if (Calc.sub(5,2).equals("3"))
            log(true, "Test 1");
        else
            log(false, "Test 1");
            
        if (Calc.sub(64,37).equals("27"))
            log(true, "Test 2");
        else
            log(false, "Test 2");
            
        if (Calc.sub(5172,6000).equals("-828"))
            log(true, "Test 3");
        else
            log(false, "Test 3");
    }
    
    private static void testMultInt(){
        System.out.println("Multiplication Int testing: ");
        
        if (Calc.mult(5,2).equals("10"))
            log(true, "Test 1");
        else
            log(false, "Test 1");
            
        if (Calc.mult(47,68).equals("3196"))
            log(true, "Test 2");
        else
            log(false, "Test 2");
            
        if (Calc.mult(843,-234).equals("-197262"))
            log(true, "Test 3");
        else
            log(false, "Test 3");
    }
    
    private static void testDivInt(){
        System.out.println("Division Int testing: ");
        
        if (Calc.div(6,0).equals("You can't do this!"))
            log(true, "Test 1");
        else
            log(false, "Test 1");
            
        if (Calc.div(-128,16).equals("-8"))
            log(true, "Test 2");
        else
            log(false, "Test 2");
            
        if (Calc.div(49305,15).equals("3287"))
            log(true, "Test 3");
        else
            log(false, "Test 3");
    }
    
    
    private static void testAddDouble(){
       Calc c = new Calc();
       System.out.println("Addition Double testing:");
        
        c.setType(false);
        
        if (c.add(0.45,0).equals("Double is not supported!"))
            log(true, "Test 1");
        else
            log(false, "Test 1");
            
        c.setType(true);
        
        if (Math.abs(Double.valueOf(c.add(50.5 , 51.123)) - 101.623) < 0.001)
            log(true, "Test 2");
        else
            log(false, "Test 2");
            
        if (c.add(Double.MAX_VALUE, Double.MAX_VALUE).equals("Too much for this type!"))
            log(true, "Test 3");
        else
            log(false, "Test 3");
       
    }
    
    private static void testSubDouble(){
       Calc c = new Calc();
       System.out.println("Subtraction Double testing:");
        
        c.setType(false);
        
        if (c.sub(0.45,0).equals("Double is not supported!"))
            log(true, "Test 1");
        else
            log(false, "Test 1");
            
        c.setType(true);
        
        if (Math.abs(Double.valueOf(c.sub(51.5 , 51.1)) - 0.4) < 0.001)
            log(true, "Test 2");
        else
            log(false, "Test 2");
            
        if (Math.abs(Double.valueOf(c.sub(0.3, 0.12)) - 0.18) < 0.001)
            log(true, "Test 3");
        else
            log(false, "Test 3");
       
    }

    private static void testMultDouble(){
       Calc c = new Calc();
       System.out.println("Multiplication Double testing:");
        
        c.setType(false);
        
        if (c.mult(0.45,0.1).equals("Double is not supported!"))
            log(true, "Test 1");
        else
            log(false, "Test 1");
            
        c.setType(true);
        
        if (c.mult(51.5 , 0.0).equals("0.0"))
            log(true, "Test 2");
        else
            log(false, "Test 2");
            
        if (Math.abs(Double.valueOf(c.mult(0.3, 0.12)) - 0.036) < 0.001)
            log(true, "Test 3");
        else
            log(false, "Test 3");
       
    }
    
    private static void testDivDouble(){
       Calc c = new Calc();
       System.out.println("Division Double testing:");
        
        c.setType(false);
        
        if (c.div(0.45,0.1).equals("Double is not supported!"))
            log(true, "Test 1");
        else
            log(false, "Test 1");
            
        c.setType(true);
        
        if (c.div(51.5 , 0).equals("You can't do this!"))
            log(true, "Test 2");
        else
            log(false, "Test 2");
            
        if (Math.abs(Double.valueOf(c.div(0.3, 0.1)) - 3.0) < 0.001)
            log(true, "Test 3");
        else
            log(false, "Test 3");
       
    }
    
    private static void log(boolean status, String test){
        if(status == true)
             System.out.println("\t" + test + ": OK");
        else
             System.err.println("\t" + test + ": Failed");
    }
}